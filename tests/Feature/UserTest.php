<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{

    public $user;
    protected function setUp(): void
    {

        $this->user = ['name' => 'ola james', 'email' => 'test@test.com', 'password'=>'1234567'];
        parent::setUp();
    }


    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_creation()
    {
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('POST', '/api/v1/user', $this->user);

        $response
            ->assertStatus(201);
    }

    public function test_invalid_data_user_creation() {
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('POST', '/api/v1/user', []);

        $response
            ->assertStatus(422);
    }

    public function test_double_registration() {
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('POST', '/api/v1/user', $this->user);

        $response
            ->assertStatus(400);
    }

    public function test_with_wrong_password() {
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('POST', '/api/v1/user/signin', ['email' => $this->user['email'], 'password' => '1234']);

        $response
            ->assertStatus(401)
            ->assertJsonCount(1);
    }
    public function test_user_authentication() {
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('POST', '/api/v1/user/signin', ['email' => $this->user['email'], 'password' =>$this->user['password']]);

        $response
            ->assertStatus(200)
            ->assertJsonCount(3);
        $this->artisan('migrate:refresh');
    }



}
