<?php

namespace Tests\Feature;

use App\Model\Profile;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use JWTAuth;
use JWT;

class ProfileTest extends TestCase
{
    public $profile, $user, $token, $credentials;

    use RefreshDatabase;
    protected function setUp():void
    {
        parent::setUp();
        $user = User::create(['name' => 'ola james', 'email' => 'test1@test.com', 'password'=>'1234567']);
        $this->token = JWTAuth::fromUser($user);
        #dump($this->token);
        $this->profile = ['sex' => 'Male', 'address' => 'no 5 obanikoro road', 'phone' => '08132193617', 'user_id'=>1, 'id' => 1];

    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_create_new_profile_correctly()
    {
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$this->token,
        ])->json('POST', '/api/v1/user-profile', $this->profile);

        $response
            ->assertStatus(201)
        ->assertJsonCount(7);
    }

    public function test_create_new_profile_incorrectly()
    {
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$this->token,
        ])->json('POST', '/api/v1/user-profile', []);

        $response
            ->assertStatus(422)
            ->assertJsonCount(2);
    }

    public function test_create_new_profile_duplicate()
    {
        Profile::create($this->profile);
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$this->token,
        ])->json('POST', '/api/v1/user-profile', $this->profile);

        $response
            ->assertStatus(422)
            ->assertJsonCount(2);
    }

    public function test_get_one_profile()
    {
        $profile = Profile::create($this->profile);
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$this->token,
        ])->json('GET', '/api/v1/user-profile/'.$profile->id);

        $response
            ->assertStatus(200)
            ->assertJsonCount(7);

    }

    public function test_get_non_existing_profile()
    {
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$this->token,
        ])->json('Get','/api/v1/user-profile/5');

        $response
            ->assertStatus(404);

        $this->artisan('migrate:refresh');
    }


}
