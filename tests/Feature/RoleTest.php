<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use JWTAuth;
use JWT;

class RoleTest extends TestCase
{
    use RefreshDatabase;
    public $role, $user, $token;
    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:seed --class=RoleTableSeeder');
        $user = User::create(['name' => 'ola james', 'email' => 'test1@test.com', 'password'=>'1234567']);
        $user->roles()->attach(1);
        $this->token = JWTAuth::fromUser($user);
        #dump($this->token);
        $this->role = ['role' => 'Male'];
    }

    public function test_create_new_profile_correctly()
    {
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$this->token,
        ])->json('POST', '/api/v1/user-roles', $this->role);

        $response
            ->assertStatus(201);
    }


    /**
     * A basic feature test example.
     *
     * @return void
     */

}
