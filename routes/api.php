<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::post('user', 'Users\AuthenticationController@createUser');
    Route::post('user/signin', 'Users\AuthenticationController@login');
    Route::post('user/assign', 'Users\AuthenticationController@attachRole');
    Route::resource('user-roles', 'Users\RolesController', ['middleware' => ['auth.jwt', 'admin']]);
    Route::resource('user-profile', 'Users\ProfileController', ['middleware' => ['auth.jwt']]);
    Route::resource('admin/productcategory', 'Product\ProductCategoryController', ['middleware' => ['auth.jwt', 'admin']]);
    Route::resource('store/product', 'Product\ProductController', ['middleware' => ['auth.jwt', 'seller']]);

});
