<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => bcrypt('password'),
            'id' => 1
        ],
            ['name' => 'ola james', 'email' => 'test@test.com', 'password'=>bcrypt('1234567'), 'id' => 1]);
    }
}
