<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Model\Profile;
use Faker\Generator as Faker;

$factory->define(Profile::class, function (Faker $faker) {
    return [
        'sex' => 'Female',
        'phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'user_id' => 1

    ];
});
