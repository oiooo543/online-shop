<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['sex', 'address', 'phone', 'user_id'];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
