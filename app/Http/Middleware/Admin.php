<?php


namespace App\Http\Middleware;


use Closure;
class Admin
{
    public function handle($request, Closure $next)
    {


        if ( auth()->user()->is('admin') )
        {
            return $next($request);
        }

        return response()->json('not authorized', 401);
    }
}
