<?php


namespace App\Http\Middleware;

use Closure;
class Seller
{
    public function handle($request, Closure $next)
    {


        if ( auth()->user()->is('seller') || auth()->user()->is('admin'))
        {
            return $next($request);
        }

        return response()->json('not authorized', 401);
    }
}
