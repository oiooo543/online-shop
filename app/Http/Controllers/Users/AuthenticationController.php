<?php


namespace App\Http\Controllers\Users;


use App\Mail\SignUp;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class AuthenticationController
{



    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user = User::with('roles')->where('email', request(['email']))->get();

        return $this->respondWithToken(['token' => $token, 'user' => $user]);
    }

    public function createUser(Request $request){


        $request->validate([
            'email' => 'required | email',
            'password' => 'required',
            'name' => 'required'
        ]);


        $user = new User($request->all());
        $user->password =  bcrypt($request->input('password'));

        try{
        if ($user->save()){
            Mail::to($user->email)->send(new SignUp($user));
            return response()->json($user, 201);
        }}catch (\Exception $exception){
            return response()->json('Error creating user', 400);
        }

        return response()->json('error', 400);
    }


    public function attachRole() {
        $user = User::findOrFail(request('user_id'));

        $user->roles()->attach(request('role_id'));


        return response()->json('', 200);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
