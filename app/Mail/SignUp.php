<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SignUp extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;
    public function __construct(User $user)
    {
        $this->data = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {


        $address = $this->data->email;
        $subject = 'Welcome to our Store!';
        $name = $this->data->name;

        return $this->view('emails.signup')
            ->subject($subject)
            ->with([ 'address' => $address, 'name' => $name ]);
    }
}
